from django.contrib import admin

# Register your models here.
from .models import Receipt
from .models import ExpenseCategory
from .models import Account


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "owner"]


admin.site.register(ExpenseCategory)


class AccountAdmin(admin.ModelAdmin):
    list_display = ["name", "number", "owner"]


admin.site.register(Account)


class ReceiptAdmin(admin.ModelAdmin):
    list_display = ["vendor", "total", "tax", "date", "purchaser", "account"]


admin.site.register(Receipt)
